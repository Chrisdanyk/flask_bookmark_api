import validators
from flasgger import swag_from
from flask import Blueprint, jsonify, request
from flask_jwt_extended import get_jwt_identity, jwt_required

from src.constants.http_status_code import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_409_CONFLICT,
)
from src.database import Bookmark, db

bookmarks = Blueprint('bookmarks', __name__, url_prefix='/api/v1/bookmarks')


@bookmarks.route('/', methods=['GET', 'POST'])
@jwt_required()
@swag_from('./docs/bookmarks/add_bookmark.yaml', methods=['POST'])
@swag_from('./docs/bookmarks/get_bookmarks.yaml', methods=['GET'])
def create_get_bookmarks():
    current_user = get_jwt_identity()
    if request.method == 'POST':
        body = request.json.get('body', ' ')
        url = request.json.get('url', ' ')

        if not validators.url(url):
            return jsonify({'error': 'Invalid Url'}), HTTP_400_BAD_REQUEST
        if Bookmark.query.filter_by(url=url).first():
            return jsonify({'error': 'Url already exist'}), HTTP_409_CONFLICT
        bookmark = Bookmark(url=url, body=body, user_id=current_user)
        db.session.add(bookmark)
        db.session.commit()
        return jsonify({
            'id': bookmark.id,
            'url': bookmark.url,
            'short_url': bookmark.short_url,
            'body': bookmark.body,
            'user_id': bookmark.user_id,
            'visits': bookmark.visits,
            'created_at': bookmark.created_at,
            'updated_at': bookmark.updated_at,
        }), HTTP_201_CREATED
    if request.method == 'GET':
        page = request.args.get('page', 1, type=int)
        per_page = request.args.get('per_page', 5, type=int)
        bookmarks = Bookmark.query.filter_by(
            user_id=current_user).paginate(
            page=page, per_page=per_page)
        data = []
        for bookmark in bookmarks.items:
            data.append({
                'id': bookmark.id,
                'url': bookmark.url,
                'short_url': bookmark.short_url,
                'body': bookmark.body,
                'user_id': bookmark.user_id,
                'visits': bookmark.visits,
                'created_at': bookmark.created_at,
                'updated_at': bookmark.updated_at,
            })
        meta = {
            'has_next': bookmarks.has_next,
            'has_prev': bookmarks.has_prev,
            'next_page': bookmarks.next_num,
            'prev_page': bookmarks.prev_num,
            'page': bookmarks.page,
            'pages': bookmarks.pages,
            'total_count': bookmarks.total
        }
        return jsonify({'meta': meta, 'data': data}), HTTP_200_OK


@bookmarks.get('/<int:id>')
@jwt_required()
def get_bookmark(id):
    current_user = get_jwt_identity()
    bookmark = Bookmark.query.filter_by(user_id=current_user, id=id).first()
    if not bookmark:
        return jsonify({
            'error': 'Item not found'
        }), HTTP_404_NOT_FOUND
    return jsonify({
        'id': bookmark.id,
        'url': bookmark.url,
        'short_url': bookmark.short_url,
        'body': bookmark.body,
        'user_id': bookmark.user_id,
        'visits': bookmark.visits,
        'created_at': bookmark.created_at,
        'updated_at': bookmark.updated_at,
    }), HTTP_200_OK


@bookmarks.route('/<int:id>', methods=['PUT', 'PATCH'])
@jwt_required()
def update_bookmark(id):
    current_user = get_jwt_identity()

    body = request.json.get('body', ' ')
    url = request.json.get('url', ' ')
    bookmark = Bookmark.query.filter_by(user_id=current_user, id=id).first()
    if not bookmark:
        return jsonify({
            'error': 'Item not found'
        }), HTTP_404_NOT_FOUND

    if not validators.url(url):
        return jsonify({'error': 'Invalid Url'}), HTTP_400_BAD_REQUEST
    if Bookmark.query.filter_by(url=url).first():
        return jsonify({'error': 'Url already exist'}), HTTP_409_CONFLICT
    bookmark.url = url
    bookmark.body = body
    db.session.commit()
    return jsonify({
        'id': bookmark.id,
        'url': bookmark.url,
        'short_url': bookmark.short_url,
        'body': bookmark.body,
        'user_id': bookmark.user_id,
        'visits': bookmark.visits,
        'created_at': bookmark.created_at,
        'updated_at': bookmark.updated_at,
    }), HTTP_200_OK


@bookmarks.delete('/<int:id>')
@jwt_required()
def delete_bookmark(id):
    current_user = get_jwt_identity()
    bookmark = Bookmark.query.filter_by(user_id=current_user, id=id).first()
    if not bookmark:
        return jsonify({
            'error': 'Item not found'
        }), HTTP_404_NOT_FOUND
    db.session.delete(bookmark)
    db.session.commit()
    return jsonify({}), HTTP_204_NO_CONTENT


@bookmarks.get('/stats')
@jwt_required()
@swag_from('./docs/bookmarks/stats.yaml')
def get_stats():
    current_user = get_jwt_identity()
    page = request.args.get('page', 1, type=int)
    per_page = request.args.get('per_page', 2, type=int)
    bookmarks_list = Bookmark.query.filter_by(
        user_id=current_user).paginate(
        page=page, per_page=per_page)
    data = []
    for item in bookmarks_list.items:
        data.append({
            'id': item.id,
            'url': item.url,
            'short_url': item.short_url,
            'visits': item.visits
        })
    meta = {
        'has_next': bookmarks_list.has_next,
        'has_prev': bookmarks_list.has_prev,
        'next_page': bookmarks_list.next_num,
        'prev_page': bookmarks_list.prev_num,
        'page': bookmarks_list.page,
        'pages': bookmarks_list.pages,
        'total_count': bookmarks_list.total
    }
    return jsonify({'data': data, 'meta': meta})
