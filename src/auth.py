import validators
from flasgger import swag_from
from flask import Blueprint, jsonify, request
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    get_jwt_identity,
    jwt_required,
)
from werkzeug.security import check_password_hash, generate_password_hash

from src.constants.http_status_code import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_400_BAD_REQUEST,
    HTTP_401_UNAUTHORIZED,
    HTTP_409_CONFLICT,
)
from src.database import User, db

auth = Blueprint('auth', __name__, url_prefix='/api/v1/auth')


@auth.post('/register')
@swag_from('./docs/auth/register.yaml')
def register():
    username = request.json['username']
    email = request.json['email']
    password = request.json['password']

    if len(password) < 6:
        return jsonify({'error': 'Password is too short'}),\
            HTTP_400_BAD_REQUEST
    if len(username) < 3:
        return jsonify({'error': 'Username is too short'}),\
            HTTP_400_BAD_REQUEST
    if not username.isalnum() or " " in username:
        return jsonify({'error': 'Username should be \
                        alphanumeric also no spaces in it'}),\
            HTTP_400_BAD_REQUEST
    if not validators.email(email):
        return jsonify({'error': 'Email is not valid'}),\
            HTTP_400_BAD_REQUEST
    if User.query.filter_by(email=email).first() is not None:
        return jsonify({"error": "Email is taken"}), HTTP_409_CONFLICT
    if User.query.filter_by(username=username).first() is not None:
        return jsonify({"error": "Username is taken"}), HTTP_409_CONFLICT
    pwd_hash = generate_password_hash(password=password)
    user = User(username=username, email=email, password=pwd_hash)
    db.session.add(user)
    db.session.commit()

    return jsonify({
        'message': 'User Created',
        'user': {'username': username},
    }), HTTP_201_CREATED


@auth.post('/login')
@swag_from('./docs/auth/login.yaml')
def login():
    username = request.json.get("username", None)
    password = request.json.get("password", None)
    user = User.query.filter_by(username=username).first()
    if user:
        is_pass_correct = check_password_hash(user.password, password)
        if is_pass_correct:
            access_token = create_access_token(identity=user.id)
            refresh_token = create_refresh_token(identity=user.id)
            return jsonify({
                'user': {
                    "access": access_token,
                    "refresh": refresh_token,
                    "username": user.username,
                    "email": user.email
                }
            }), HTTP_200_OK
    return jsonify({"error": "Wrong Credentials"}), HTTP_401_UNAUTHORIZED


@auth.post('/me')
@jwt_required()
def get_profile():
    user_id = get_jwt_identity()
    user = User.query.filter_by(id=user_id).first()
    return jsonify(
        {'username': user.username,
         'email': user.email,
         }
    ), HTTP_200_OK


@auth.post('/token/refresh')
@jwt_required(refresh=True)
def refresh_users_token():
    identity = get_jwt_identity()
    access_token = create_access_token(identity=identity)
    return jsonify(
        {'access': access_token
         }
    ), HTTP_200_OK
