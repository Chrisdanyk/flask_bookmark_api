export FLASK_ENV = development
export FLASK_APP = src
export SQLALCHEMY_DB_URI = 'sqlite:///bookmarks.db'
export FLASK_DEBUG = 1
export SECRET_KEY = 'should-change-secret-key'
export JWT_SECRET_KEY = 'should-change-jwt-secret-key'